﻿CREATE TABLE [dbo].[Person]
(
		[Id]		INT			NOT NULL	PRIMARY KEY	IDENTITY
	,	[FirstName]	VARCHAR(50)	NOT NULL
	,	[LastName]	VARCHAR(50)	NOT NULL
	,	[AdditionalField1] NCHAR(10) NULL
	,	[AdditionalField2] NCHAR(10) NULL
	,	[AdditionalField3] NCHAR(10) NULL
	,	[AdditionalField4] NCHAR(10) NULL
)
