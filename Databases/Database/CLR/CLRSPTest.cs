using System;
using Microsoft.SqlServer.Server;

namespace DeviousCreation.Playground.Database.CLR
{
	public partial class StoredProcedures
	{
		[SqlProcedure]
		public static void ClrspTest()
		{
			SqlPipe sp = SqlContext.Pipe;
			String strCurrentTime = "At the moment the current System DateTime is: " + DateTime.Now;
			sp.Send(strCurrentTime);
		}
	}
}