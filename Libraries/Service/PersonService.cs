﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeviousCreation.Playground.Service
{
	using System.Dynamic;
	using Domain.Entities;
	using Domain.InfrastructureModels;
	using Domain.Repositories;
	using Domain.Services;

	public class PersonService : IPersonService
	{
		private readonly IUoW _uow;
		public PersonService(IUoW uow)
		{
			_uow = uow;
		}

		/// <summary>
		///     Creates the specified entity.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		public ServiceResult<Person> Create(Person entity)
		{
			_uow.Persons.Add(entity);
			return new ServiceResult<Person> { ValidationResults = _uow.Commit(), ReturnObject = entity };
		}

		/// <summary>
		///     Selects all.
		/// </summary>
		/// <returns></returns>
		public IQueryable<Person> SelectAll()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		///     Selects the by id.
		/// </summary>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		public Person SelectById(int id)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		///     Updates the specified entity.
		/// </summary>
		/// <param name="entity">The entity.</param>
		public ServiceResult<Person> Update(Person entity)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		///     Deletes the specified entity.
		/// </summary>
		/// <param name="entity">The entity.</param>
		public ServiceResult<Person> Delete(Person entity)
		{
			throw new NotImplementedException();
		}

		public IEntityWatcher<Person> GetWatcher()
		{
			var query = from p in _uow.Persons.GetAll()
							select  p;


			return _uow.Persons.GenerateEntityWatcher(query);
		}
	}
}
