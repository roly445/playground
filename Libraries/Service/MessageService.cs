﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeviousCreation.Playground.Service
{
	using Domain.Entities;
	using Domain.InfrastructureModels;
	using Domain.Repositories;
	using Domain.Services;

	public class MessageService : IMessageService
	{
		private readonly IUoW _uow;
		public MessageService(IUoW uow)
		{
			_uow = uow;
		}

		/// <summary>
		///     Creates the specified entity.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		public ServiceResult<Message> Create(Message entity)
		{
			_uow.Messages.Add(entity);
			return new ServiceResult<Message> { ValidationResults = _uow.Commit(), ReturnObject = entity };
		}

		/// <summary>
		///     Selects all.
		/// </summary>
		/// <returns></returns>
		public IQueryable<Message> SelectAll()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		///     Selects the by id.
		/// </summary>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		public Message SelectById(int id)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		///     Updates the specified entity.
		/// </summary>
		/// <param name="entity">The entity.</param>
		public ServiceResult<Message> Update(Message entity)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		///     Deletes the specified entity.
		/// </summary>
		/// <param name="entity">The entity.</param>
		public ServiceResult<Message> Delete(Message entity)
		{
			throw new NotImplementedException();
		}
	}
}
