﻿namespace DeviousCreation.Playground.Domain.InfrastructureModels
{
	using System.Collections.Generic;
	using DeviousCreation.Playground.Domain.Entities;

	public class ServiceResult<T> where T : BaseEntity
	{
		public IList<ValidationFailure> ValidationResults { get; set; }
		public T ReturnObject { get; set; }
	}
}