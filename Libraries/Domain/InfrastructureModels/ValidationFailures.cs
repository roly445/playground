﻿namespace DeviousCreation.Playground.Domain.InfrastructureModels
{
	using System.Collections.Generic;
	using System.Text;

	public class ValidationFailures : List<ValidationFailure>
    {
        public bool IsValid { get; set; }

        public ValidationFailures()
        {
            IsValid = true;
        }

        public override string ToString()
        {
            var errors = new StringBuilder();
            foreach (var failure in this)
            {
                errors.AppendLine(failure.ErrorMessage);
            }
            return errors.ToString();
        }

        public string ToHtmlString()
        {
            var errors = new StringBuilder();
            foreach (var failure in this)
            {
                errors.AppendLine(failure.ErrorMessage + "<br />");
            }
            return errors.ToString();
        }
    }
}
