﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeviousCreation.Playground.Domain.Entities
{
	using System.Data;
	using System.Data.Entity;
	using System.Data.Entity.Validation;

	public class Message : BaseEntity, IEntityValidatableObject<Message>
	{
		public string Content { get; set; }
		public void Validate(DbEntityValidationResult dbEntityValidationResult, DbSet<Message> dbSet, EntityState state)
		{
			
		}
	}
}
