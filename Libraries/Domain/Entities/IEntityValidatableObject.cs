﻿namespace DeviousCreation.Playground.Domain.Entities
{
	using System.Data;
	using System.Data.Entity;
	using System.Data.Entity.Validation;

	public interface IEntityValidatableObject<T> where T : class
    {
        void Validate(DbEntityValidationResult dbEntityValidationResult, DbSet<T> dbSet, EntityState state);
    }
}
