﻿namespace DeviousCreation.Playground.Domain.Entities
{
    /// <summary>
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        ///     Gets or sets the id.
        /// </summary>
        /// <value>
        ///     The id.
        /// </value>
        public int Id { get; set; }
    }
}