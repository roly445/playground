﻿namespace DeviousCreation.Playground.Domain.Entities
{
	using System;
	using System.ComponentModel.DataAnnotations;
	using System.Data;
	using System.Data.Entity;
	using System.Data.Entity.Validation;

	public class Person : BaseEntity, IEntityValidatableObject<Person>
	{
		[Required]
		[MaxLength(50)]
		public string FirstName { get; set; }

		[Required]
		[MaxLength(50)]
		public string LastName { get; set; }

		public void Validate(DbEntityValidationResult dbEntityValidationResult, DbSet<Person> dbSet, EntityState state)
		{
			throw new NotImplementedException();
		}
	}
}