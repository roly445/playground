﻿namespace DeviousCreation.Playground.Domain.Repositories
{
	using System;
	using System.Data.Entity;

	/// <summary>
    /// </summary>
    public interface IRepositoryProvider
    {
        /// <summary>
        ///     Gets or sets the db context.
        /// </summary>
        /// <value>
        ///     The db context.
        /// </value>
        DbContext DbContext { get; set; }

        #region Methods

        /// <summary>
        ///     Gets the type of the repository for entity.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        IRepository<T> GetRepositoryForEntityType<T>() where T : class;

        /// <summary>
        ///     Gets the repository.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="factory">The factory.</param>
        /// <returns></returns>
        T GetRepository<T>(Func<DbContext, object> factory = null) where T : class;

        #endregion
    }
}