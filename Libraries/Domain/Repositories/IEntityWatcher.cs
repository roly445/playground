﻿namespace DeviousCreation.Playground.Domain.Repositories
{
	using System;

	public interface IEntityWatcher<T> where T : class
	{
		event EventHandler OnChanged;
		bool StopSqlDependency();
	}
}
