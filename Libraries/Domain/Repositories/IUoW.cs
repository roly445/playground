﻿namespace DeviousCreation.Playground.Domain.Repositories
{
	using System.Linq;
	using DeviousCreation.Playground.Domain.InfrastructureModels;
	using Entities;

	/// <summary>
    /// </summary>
    public interface IUoW
    {
        /// <summary>
        ///     Gets the users.
        /// </summary>
        /// <value>
        ///     The users.
        /// </value>
		IRepository<Person> Persons { get; }

		IRepository<Message> Messages { get; }

		#region Methods

        /// <summary>
        ///     Commits this instance.
        /// </summary>
        ValidationFailures Commit();

        #endregion

    }
}