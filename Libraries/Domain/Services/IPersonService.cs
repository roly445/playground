﻿namespace DeviousCreation.Playground.Domain.Services
{
	using System.Linq;
	using Entities;
	using Repositories;

	public interface IPersonService : IService<Person>
	{
		IEntityWatcher<Person> GetWatcher();
	}
}