﻿namespace DeviousCreation.Playground.Domain.Services
{
	using System.Linq;
	using DeviousCreation.Playground.Domain.Entities;
	using DeviousCreation.Playground.Domain.InfrastructureModels;

	/// <summary>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IService<T> where T : BaseEntity
    {
        #region Methods

	    /// <summary>
	    ///     Creates the specified entity.
	    /// </summary>
	    /// <param name="entity">The entity.</param>
	    /// <returns></returns>
	    ServiceResult<T> Create(T entity);

        /// <summary>
        ///     Selects all.
        /// </summary>
        /// <returns></returns>
        IQueryable<T> SelectAll();

        /// <summary>
        ///     Selects the by id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        T SelectById(int id);

        /// <summary>
        ///     Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
		ServiceResult<T> Update(T entity);

        /// <summary>
        ///     Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
		ServiceResult<T> Delete(T entity);

        #endregion
    }
}