﻿namespace DeviousCreation.Playground.Domain.Services
{
	using Entities;

	public interface IMessageService : IService<Message> {}
}