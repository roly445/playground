﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServiceDebugger
{
	using System.ServiceProcess;

	public partial class ServiceRunner : Form
	{
		private readonly IDebuggableService _debuggableService;

		public ServiceRunner(IDebuggableService service)
		{
			InitializeComponent();
			_debuggableService = service;
			ServiceBase winService = _debuggableService as ServiceBase;
			if (winService != null) Text = winService.ServiceName + " Controler";
			Show();
		}

		private void CmdStartClick(object sender, EventArgs e)
		{
			_debuggableService.StartOverride(new string[] { });
			lblStatus.Text = "Started";
		}

		private void CmdStopClick(object sender, EventArgs e)
		{
			_debuggableService.StopOverride();
			lblStatus.Text = "Stopped";
		}
	}
}
