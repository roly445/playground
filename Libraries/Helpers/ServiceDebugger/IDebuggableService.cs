﻿namespace ServiceDebugger
{
	public interface IDebuggableService
	{
		void StartOverride(string[] args);
		void StopOverride();
	}
}