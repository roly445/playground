﻿namespace DeviousCreation.Playground.Repository
{
	using System;
	using System.Data.Entity;
	using System.Data.Entity.Infrastructure;
	using System.Data.EntityClient;
	using System.Data.Objects;
	using System.Data.SqlClient;
	using System.Linq;
	using Domain.Repositories;

	public class EntityWatcher<T> : IDisposable, IEntityWatcher<T> where T : class
	{
		private SqlConnection _connection;
		private SqlCommand _command;
		private IQueryable _iquery;
		private readonly ObjectQuery _oquery;
		private SqlDependency _dependency;

		public event EventHandler OnChanged;

		#region Helpers

		private void GetSqlCommand(ObjectQuery query, ref SqlConnection connection, ref SqlCommand command)
		{
			if (query == null)
				throw new System.ArgumentException("Paramter cannot be null", "query");

			if (connection == null)
			{
				connection = new SqlConnection(GetConnectionString(query));
			}

			if (command == null)
			{
				command = new SqlCommand(GetSqlString(query), connection);

				// Add all the paramters used in query.
				foreach (ObjectParameter parameter in query.Parameters)
				{
					command.Parameters.AddWithValue(parameter.Name, parameter.Value);
				}
			}
		}

		private static string GetConnectionString(ObjectQuery query)
		{
			if (query == null)
			{
				throw new ArgumentException("Paramter cannot be null", "query");
			}

			EntityConnection connection = query.Context.Connection as EntityConnection;
			return connection.StoreConnection.ConnectionString;
		}

		private static string GetSqlString(ObjectQuery query)
		{
			if (query == null)
			{
				throw new ArgumentException("Paramter cannot be null", "query");
			}

			string s = query.ToTraceString();
			return s;
		}

		#endregion

		
		public EntityWatcher(DbContext context, IQueryable query)
		{
			try
			{
				_iquery = query;

				// Get the ObjectQuery directly or convert the DbQuery to ObjectQuery.
				_oquery = GetObjectQuery(context, _iquery);

				GetSqlCommand(_oquery, ref _connection, ref _command);

				BeginSqlDependency();
			}
			catch (ArgumentException ex)
			{
				if (ex.ParamName == "context")
				{
					throw new ArgumentException("Paramter cannot be null", "context", ex);
				}
				else
				{
					throw new ArgumentException("Paramter cannot be null", "query", ex);
				}
			}
			catch (Exception ex)
			{
				throw new Exception(
						"Fails to initialize a new instance of EntityWatcher class.", ex);
			}
		}

		private ObjectQuery GetObjectQuery(DbContext context, IQueryable query)
		{
			if (query is ObjectQuery)
				return query as ObjectQuery;

			if (context == null)
				throw new ArgumentException("Paramter cannot be null", "context");

			// Use the DbContext to create the ObjectContext
			ObjectContext objectContext = ((IObjectContextAdapter)context).ObjectContext;
			// Use the DbSet to create the ObjectSet and get the appropriate provider.
			IQueryable iqueryable = objectContext.CreateObjectSet<T>() as IQueryable;
			IQueryProvider provider = iqueryable.Provider;

			// Use the provider and expression to create the ObjectQuery.
			return provider.CreateQuery(query.Expression) as ObjectQuery;
		}

		private void BeginSqlDependency()
		{
			// Before start the SqlDependency, stop all the SqlDependency.
			SqlDependency.Stop(GetConnectionString(_oquery));
			SqlDependency.Start(GetConnectionString(_oquery));

			RegisterSqlDependency();
		}

		private void RegisterSqlDependency()
		{
			if (_command == null || _connection == null)
			{
				throw new ArgumentException("command and connection cannot be null");
			}

			// Make sure the command object does not already have
			// a notification object associated with it.
			_command.Notification = null;

			// Create and bind the SqlDependency object to the command object.
			_dependency = new SqlDependency(_command);
			_dependency.OnChange += new OnChangeEventHandler(DependencyOnChange);

			// After register SqlDependency, the SqlCommand must be executed, or we can't 
			// get the notification.
			RegisterSqlCommand();
		}

		private void DependencyOnChange(object sender, SqlNotificationEventArgs e)
		{
			// Move the original SqlDependency event handler.
			SqlDependency dependency = (SqlDependency)sender;
			dependency.OnChange -= DependencyOnChange;

			if (OnChanged != null)
			{
				OnChanged(this, null);
			}

			// We re-register the SqlDependency.
			RegisterSqlDependency();
		}

		private void RegisterSqlCommand()
		{
			if (_connection != null && _command != null)
			{
				_connection.Open();
				_command.ExecuteNonQuery();
				_connection.Close();
			}
		}

		/// <summary>
		/// Releases all the resources by the EntityWatcher.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected void Dispose(Boolean disposed)
		{
			if (disposed)
			{
				if (StopSqlDependency())
				{
					if (_command != null)
					{
						_command.Dispose();
						_command = null;
					}

					if (_connection != null)
					{
						_connection.Dispose();
						_connection = null;
					}

					OnChanged = null;
					_iquery = null;
					_dependency.OnChange -= DependencyOnChange;
					_dependency = null;
				}
			}
		}

		/// <summary>
		/// Stops the notification of SqlDependency.
		/// </summary>
		/// <returns>If be success, returns true;If fails, throw the exception</returns>
		public Boolean StopSqlDependency()
		{
			try
			{
				SqlDependency.Stop(GetConnectionString(_oquery));
				return true;
			}
			catch (ArgumentException ex)
			{
				throw new ArgumentException("Parameter cannot be null.", "query", ex);
			}
			catch (Exception ex)
			{
				throw new Exception("Fails to Stop the SqlDependency in the EntityWatcher class.", ex);
			}
		}
	}
}