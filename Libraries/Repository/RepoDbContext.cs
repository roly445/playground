﻿namespace DeviousCreation.Playground.Repository
{
	using System.Collections.Generic;
	using System.Data.Entity;
	using System.Data.Entity.Infrastructure;
	using System.Data.Entity.ModelConfiguration.Conventions;
	using System.Data.Entity.Validation;
	using Domain.Entities;

	/// <summary>
    /// </summary>
    public class RepoDbContext : DbContext
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="RepoDbContext" /> class.
        /// </summary>
        public RepoDbContext() : base("ConnStr") {}


		public DbSet<Person> Persons { get; set; }
		public DbSet<Message> Messages { get; set; }


        #region Methods

        /// <summary>
        ///     This method is called when the model for a derived context has been initialized, but
        ///     before the model has been locked down and used to initialize the context.  The default
        ///     implementation of this method does nothing, but it can be overridden in a derived class
        ///     such that the model can be further configured before it is locked down.
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        /// <remarks>
        ///     Typically, this method is called only once when the first instance of a derived context
        ///     is created.  The model for that context is then cached and is for all further instances of
        ///     the context in the app domain.  This caching can be disabled by setting the ModelCaching
        ///     property on the given ModelBuidler, but note that this can seriously degrade performance.
        ///     More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        ///     classes directly.
        /// </remarks>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

			modelBuilder.Entity<Person>().ToTable("Person");
			modelBuilder.Entity<Message>().ToTable("Message");



        }

        protected override DbEntityValidationResult ValidateEntity(
                DbEntityEntry entityEntry, IDictionary<object, object> items)
        {
            var result = new DbEntityValidationResult(entityEntry, new List<DbValidationError>());

            
            return result.ValidationErrors.Count > 0 ? result : base.ValidateEntity(entityEntry, items);
        }

		public override int SaveChanges()
		{
			// Need to manually delete all responses that have been removed from the patient, otherwise they'll be orphaned.
			

			return base.SaveChanges();
		}
        #endregion
    }
}