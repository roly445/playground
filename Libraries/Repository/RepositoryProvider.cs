﻿namespace DeviousCreation.Playground.Repository
{
	using System;
	using System.Collections.Generic;
	using System.Data.Entity;
	using Domain.Repositories;

	/// <summary>
    /// </summary>
    public class RepositoryProvider : IRepositoryProvider
    {
        /// <summary>
        ///     The _repository factories
        /// </summary>
        private readonly RepositoryFactories _repositoryFactories;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RepositoryProvider" /> class.
        /// </summary>
        /// <param name="repositoryFactories">The repository factories.</param>
        public RepositoryProvider(RepositoryFactories repositoryFactories)
        {
            _repositoryFactories = repositoryFactories;
            Repositories = new Dictionary<Type, object>();
        }

        /// <summary>
        ///     Gets the repositories.
        /// </summary>
        /// <value>
        ///     The repositories.
        /// </value>
        protected Dictionary<Type, object> Repositories { get; private set; }

        #region IRepositoryProvider Members

        /// <summary>
        ///     Gets or sets the db context.
        /// </summary>
        /// <value>
        ///     The db context.
        /// </value>
        public DbContext DbContext { get; set; }

        /// <summary>
        ///     Gets the type of the repository for entity.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IRepository<T> GetRepositoryForEntityType<T>() where T : class
        {
            return GetRepository<IRepository<T>>(
                _repositoryFactories.GetRepositoryFactoryForEntityType<T>());
        }

        /// <summary>
        ///     Gets the repository.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="factory">The factory.</param>
        /// <returns></returns>
        public virtual T GetRepository<T>(Func<DbContext, object> factory = null) where T : class
        {
            object repoObj;
            Repositories.TryGetValue(typeof (T), out repoObj);
            if (repoObj != null)
                return (T)repoObj;

            return MakeRepository<T>(factory, DbContext);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Makes the repository.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="factory">The factory.</param>
        /// <param name="dbContext">The db context.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException">No factory for repository type,  + typeof (T)</exception>
        protected virtual T MakeRepository<T>(Func<DbContext, object> factory, DbContext dbContext)
        {
            Func<DbContext, object> f = factory ?? _repositoryFactories.GetRepositoryFactory<T>();
            if (f == null)
                throw new NotImplementedException("No factory for repository type, " + typeof (T));

            T repo = (T)f(dbContext);
            Repositories [typeof (T)] = repo;
            return repo;
        }

        /// <summary>
        ///     Sets the repository.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="repository">The repository.</param>
        public void SetRepository<T>(T repository)
        {
            Repositories [typeof (T)] = repository;
        }

        #endregion
    }
}