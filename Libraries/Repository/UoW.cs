﻿namespace DeviousCreation.Playground.Repository
{
	using System;
	using System.Data.Entity.Validation;
	using System.Linq;
	using Domain.Entities;
	using Domain.InfrastructureModels;
	using Domain.Repositories;

	/// <summary>
    /// 
    /// </summary>
    public class UoW : IUoW, IDisposable
    {
        /// <summary>
        /// The _DB context
        /// </summary>
        private RepoDbContext _dbContext;

		/// <summary>
		/// Initializes a new instance of the <see cref="UoW" /> class.
		/// </summary>
		/// <param name="repositoryProvider">The repository provider.</param>
		/// <param name="proxyCreationEnabled">if set to <c>true</c> [proxy creation enabled].</param>
		/// <param name="lazyLoadingEnabled">if set to <c>true</c> [lazy loading enabled].</param>
		public UoW(IRepositoryProvider repositoryProvider, bool proxyCreationEnabled = true, bool lazyLoadingEnabled = true)
        {
            CreateDbContext();
			_dbContext.Configuration.ProxyCreationEnabled = proxyCreationEnabled;
			_dbContext.Configuration.LazyLoadingEnabled = lazyLoadingEnabled;
            repositoryProvider.DbContext = _dbContext;
            RepositoryProvider = repositoryProvider;

			_dbContext.Database.Initialize(true);
        }

        /// <summary>
        /// Gets or sets the repository provider.
        /// </summary>
        /// <value>
        /// The repository provider.
        /// </value>
        protected IRepositoryProvider RepositoryProvider { get; set; }

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            _dbContext.Dispose();
        }

        #endregion

        #region IUoW Members

        

        /// <summary>
        /// Gets the users.
        /// </summary>
        /// <value>
        /// The users.
        /// </value>
		public IRepository<Person> Persons { get { return GetStandardRepo<Person>(); } }

		public IRepository<Message> Messages { get { return GetStandardRepo<Message>(); } }

		/// <summary>
        /// Commits this instance.
        /// </summary>
        public ValidationFailures Commit()
        {
            var validationFailures = new ValidationFailures();
            try
            {
                _dbContext.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var entityValidationError in ex.EntityValidationErrors)
                {
                    validationFailures.AddRange(
                            entityValidationError.ValidationErrors.Select(
                                    validationError => new ValidationFailure
                                    {
                                        ErrorMessage = validationError.ErrorMessage,
                                        PropertyName = validationError.PropertyName
                                    }));
                    validationFailures.IsValid = false;
                }
            }
            return validationFailures;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates the db context.
        /// </summary>
        protected void CreateDbContext()
        {
            _dbContext = new RepoDbContext();
        }

        /// <summary>
        /// Gets the standard repo.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private IRepository<T> GetStandardRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepositoryForEntityType<T>();
        }

        /// <summary>
        /// Gets the repo.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private T GetRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepository<T>();
        }

        #endregion
    }
}