﻿namespace DeviousCreation.Playground.Repository
{
	using System;
	using System.Collections.Generic;
	using System.Data.Entity;

	/// <summary>
    /// </summary>
    public class RepositoryFactories
    {
        /// <summary>
        ///     The _repository factories
        /// </summary>
        private readonly IDictionary<Type, Func<DbContext, object>> _repositoryFactories;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RepositoryFactories" /> class.
        /// </summary>
        public RepositoryFactories()
        {
            _repositoryFactories = new Dictionary<Type, Func<DbContext, object>>();
        }

        #region Methods

        /// <summary>
        ///     Gets the repository factory.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public Func<DbContext, object> GetRepositoryFactory<T>()
        {
            Func<DbContext, object> factory;
            _repositoryFactories.TryGetValue(typeof (T), out factory);
            return factory;
        }

        /// <summary>
        ///     Gets the type of the repository factory for entity.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public Func<DbContext, object> GetRepositoryFactoryForEntityType<T>() where T : class
        {
            return GetRepositoryFactory<T>() ?? DefaultEntityRepositoryFactory<T>();
        }

        /// <summary>
        ///     Defaults the entity repository factory.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected virtual Func<DbContext, object> DefaultEntityRepositoryFactory<T>() where T : class
        {
            return dbContext => new EfRepository<T>(dbContext);
        }

        #endregion
    }
}