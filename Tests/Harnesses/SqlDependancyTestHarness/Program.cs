﻿namespace DeviousCreation.Playground.Tests.Harnesses.SqlDependancyTestHarness
{
	using System;
	using Domain.Entities;
	using Domain.Repositories;
	using Domain.Services;
	using Ninject;
	using Repository;
	using Service;

	class Program
	{
		static void Main(string[] args)
		{
			IKernel kernel = new StandardKernel();
			kernel.Bind<IPersonService>().To<PersonService>();
			
			kernel.Bind<IUoW>().To<UoW>();
			kernel.Bind<RepositoryFactories>().ToSelf().InSingletonScope();
			kernel.Bind<IRepositoryProvider>().To<RepositoryProvider>();

			var personService = kernel.Get<IPersonService>();

			var watcher = personService.GetWatcher();
			watcher.OnChanged += watcher_OnChanged;

			Console.ReadKey();
		}

		static void watcher_OnChanged(object sender, System.EventArgs e)
		{
			Console.Write("123abc");
		}
	}
}
