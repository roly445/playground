﻿namespace DeviousCreation.Playground.Windows.WindowsService
{
	using System.Net.Mime;
	using System.ServiceProcess;
	using Domain.Repositories;
	using Domain.Services;
	using Ninject;
	using Repository;
	using Service;
	using ServiceDebugger;
	using System.Windows.Forms;

	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main(string[] args)
		{
			var kernel = new StandardKernel();
			kernel.Bind<IUoW>().To<UoW>();
			kernel.Bind<RepositoryFactories>().ToSelf().InSingletonScope();
			kernel.Bind<IRepositoryProvider>().To<RepositoryProvider>();
			kernel.Bind<IMessageService>().To<MessageService>();
			kernel.Bind<Service1>().ToSelf();

			if (args.Length > 0 && args[0].ToLower().Equals("/debug"))
			{
				Application.Run(new ServiceRunner(kernel.Get<Service1>()));
				//Application.Run(new ServiceRunner(new ServiceSample2()));
			}
			ServiceBase[] servicesToRun =
			{ 
				kernel.Get<Service1>()
			};
			ServiceBase.Run(servicesToRun);
		}
	}
}
