﻿namespace DeviousCreation.Playground.Windows.WindowsService
{
	using System;
	using System.ServiceProcess;
	using System.Timers;
	using Domain.Entities;
	using Domain.Services;
	using ServiceDebugger;

	public partial class Service1 : ServiceBase, IDebuggableService
	{
		private readonly Timer _timer;
		private readonly IMessageService _messageService;

		public Service1(IMessageService messageService)
		{
			_messageService = messageService;
			InitializeComponent();
			_timer = new Timer();
			_timer.Elapsed += TimerElapsed;
			_timer.Interval = 60000;
		}

		private void TimerElapsed(object sender, ElapsedEventArgs e)
		{
			_messageService.Create(
					new Message
					{
						Content = "Created at " + DateTime.Now
					});
		}

		protected override void OnStart(string[] args)
		{
			_timer.Start();
		}

		protected override void OnStop()
		{
			_timer.Stop();
		}

		public void StartOverride(string[] args)
		{
			OnStart(args);
		}

		public void StopOverride()
		{
			OnStop();
		}
	}
}